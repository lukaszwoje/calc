import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;


public class CalculateTest {
    private static Calculate calculate;

    @BeforeClass
    public static void setupClass() {
        System.out.println("setup class");
        calculate = new Calculate();
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void throwsIOExceptionIfFileIsNotInDirectory(){
        Path resourcePath = Paths.get("src", "main", "resources");
        Path pathToFIle = resourcePath.resolve("phrases.txt");
    exception.expect(IOException.class);
    exception.expectMessage("Text file does not exist");
    calculate.readMathematicalExpressionFromFile(pathToFIle);
    }



    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionForNullInPath() {
        Calculate calculate = new Calculate();
        calculate.readMathematicalExpressionFromFile(null);
    }
}
