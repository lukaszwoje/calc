import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Calculate {

    TextBinaryOperators textBinaryOperators = new TextBinaryOperators();
    BinaryOperators binaryOperators = new BinaryOperators();

    public String readMathematicalExpressionFromFile(Path pathToFIle) {

        String mathematicalExpression = "";
        StringBuilder sb;
        String line="";

        try (BufferedReader bufferedReader = Files.newBufferedReader(pathToFIle)) {
            sb = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                String[] lineSplited = line.split(" ");

                if (lineSplited[0].equals(textBinaryOperators.getAdd())) {
                    sb.append(binaryOperators.getAddOperator()).append(lineSplited[1]);
                } else if (lineSplited[0].equals(textBinaryOperators.getSubstract())) {
                    sb.append(binaryOperators.getSubstractOperator()).append(lineSplited[1]);
                } else if (lineSplited[0].equals(textBinaryOperators.getMultiply())) {
                    sb.append(binaryOperators.getMultiplyOperator()).append(lineSplited[1]);
                } else if (lineSplited[0].equals(textBinaryOperators.getDivide())) {
                    sb.append(binaryOperators.getDivideOperator()).append(lineSplited[1]);
                } else if (lineSplited[0].equals(binaryOperators.getApply())) {
                    mathematicalExpression = lineSplited[1].concat(sb.toString());
                }
            }
        } catch (IOException e) {
            System.out.println("Text file does not exist");
        }
        return mathematicalExpression;
    }

    public int parseStringExpressionToInt(String mathematicalExpression) {
        int result = 0;
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");
        try {
            result = (int) engine.eval(mathematicalExpression);
        } catch (ScriptException e) {
            System.out.println("Script Error during interpreting expression");
        }
        return result;
    }
}
