public class TextBinaryOperators {

    private final String add = "add";
    private final String substract = "substract";
    private final String multiply = "multiply";
    private final String divide = "divide";


    public TextBinaryOperators() {
    }

    public String getAdd() {
        return add;
    }

    public String getSubstract() {
        return substract;
    }

    public String getMultiply() {
        return multiply;
    }

    public String getDivide() {
        return divide;
    }
}

