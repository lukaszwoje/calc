public class App {

    public static void main(String[] args) {
        Calculate calculate = new Calculate();
        PathFile pathFile = new PathFile();

        String expression = calculate.readMathematicalExpressionFromFile(pathFile.getPathToFIle());
        int result = calculate.parseStringExpressionToInt(expression);
        System.out.println(result);
    }
}
