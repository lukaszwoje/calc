public class BinaryOperators {
    private final String addOperator = "+";
    private final String substractOperator = "-";
    private final String multiplyOperator = "*";
    private final String divideOperator = "/";
    private final String apply = "apply";

    public BinaryOperators() {
    }

    public String getAddOperator() {
        return addOperator;
    }

    public String getSubstractOperator() {
        return substractOperator;
    }

    public String getMultiplyOperator() {
        return multiplyOperator;
    }

    public String getDivideOperator() {
        return divideOperator;
    }

    public String getApply() {
        return apply;
    }
}
